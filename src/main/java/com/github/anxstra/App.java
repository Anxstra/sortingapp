package com.github.anxstra;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main( String[] args ) {
        System.out.println("Hello User");
        System.out.println("Enter arguments( enter \"stop\" to finish)");
        Scanner scanner = new Scanner(System.in);
        List<Integer> integers = new ArrayList<>();
        String str = scanner.nextLine();
        while (!str.equalsIgnoreCase("stop")) {
            integers.add(Integer.valueOf(str));
            str = scanner.nextLine();
        }
        int[] values = integers.stream().mapToInt(i -> i).toArray();
        MathUtils.sortNumbers(values);
        System.out.println("Result:");
        for (int val : values) {
            System.out.print(val + "\t");
        }
    }
}
