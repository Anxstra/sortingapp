package com.github.anxstra;

import java.util.Arrays;

public class MathUtils {

    private MathUtils() {}
    public static void sortNumbers(int[] array) {
        if (array.length == 0)
            throw new IllegalArgumentException("Array must be present");
        if (array.length > 10)
            throw new IllegalArgumentException("Max amount of arguments is 10");
        Arrays.sort(array);
    }
}
