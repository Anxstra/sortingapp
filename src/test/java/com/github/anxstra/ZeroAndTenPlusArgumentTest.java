package com.github.anxstra;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class ZeroAndTenPlusArgumentTest {

    @Test
    public void zeroArgumentTestCase() {
        int[] in = new int[0];
        try {
            MathUtils.sortNumbers(in);
            fail("IllegalArgumentException must be thrown!");
        } catch (IllegalArgumentException ignored) {}
    }

    @Test
    public void tenPlusArgumentTestCase() {
        int[] in = new Random().ints().limit(11).toArray();
        try {
            MathUtils.sortNumbers(in);
            fail("IllegalArgumentException must be thrown!");
        } catch (IllegalArgumentException ignored) {}
    }

}
