package com.github.anxstra;

import org.junit.runner.*;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ZeroAndTenPlusArgumentTest.class, ParametrizedOneArgumentTest.class, ParametrizedTenArgumentTest.class})
public class TestSuite {
}
