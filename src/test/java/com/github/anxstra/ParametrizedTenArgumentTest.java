package com.github.anxstra;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import java.util.concurrent.ThreadLocalRandom;


@RunWith(Parameterized.class)
public class ParametrizedTenArgumentTest {
    private int[] in;
    private int[] expected;

    public ParametrizedTenArgumentTest(int[] in, int[] expected) {
        this.in = in;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        List<Object[]> source = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            Object[] data = new Object[2];
            int[] val = ThreadLocalRandom.current().ints().limit(10).toArray();
            int[] exp = Arrays.copyOf(val, val.length);
            Arrays.sort(exp);
            MathUtils.sortNumbers(val);
            data[0] = val;
            data[1] = exp;
            source.add(data);
        }
        return source;
    }
    @Test
    public void test() {
        Assert.assertArrayEquals(expected, in);
    }


}
